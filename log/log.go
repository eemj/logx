package log // import "go.jamie.mt/logx/log"

import (
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Logger struct {
	lvl zap.AtomicLevel
	log *zap.Logger
}

var (
	_globalRWMutex sync.RWMutex
	_logger        *Logger
)

func SetLevel(level zapcore.Level) {
	_globalRWMutex.Lock()
	if _logger != nil {
		_logger.lvl.SetLevel(level)
	}
	_globalRWMutex.Unlock()
}

func Create(level zapcore.Level, builders ...CoreBuilder) *Logger {
	_globalRWMutex.Lock()

	atomicLevel := zap.NewAtomicLevelAt(level)

	// Build our cores.
	cores := make([]zapcore.Core, 0, len(builders))
	for _, builder := range builders {
		cores = append(cores, builder(atomicLevel))
	}

	// Instantiate our log
	_logger = &Logger{
		lvl: atomicLevel,
		log: zap.New(
			zapcore.NewTee(cores...),
			zap.AddCaller(),
			zap.AddStacktrace(zap.LevelEnablerFunc(
				func(l zapcore.Level) bool {
					return atomicLevel.Enabled(l) && l >= zapcore.ErrorLevel
				},
			)),
		),
	}

	_globalRWMutex.Unlock()

	// Replace the default zap logger.
	zap.ReplaceGlobals(_logger.log)

	return _logger
}
