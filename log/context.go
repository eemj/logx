package log

import (
	"context"

	"go.jamie.mt/logx/fields"
	oteltrace "go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

// WithTrace uses otel.SpanContextFromContext to retrieve the `SpanContext`.
// If it's valid it will add the tracing fields and return a logger.
func WithTrace(ctx context.Context) *zap.Logger {
	return InjectActivity(ctx, getLog())
}

// InjectActivity reads trace information from the `SpanContext` and adds the
// tracing fields returning the logger.
func InjectActivity(ctx context.Context, logger *zap.Logger) *zap.Logger {
	if spanContext := oteltrace.SpanContextFromContext(ctx); spanContext.IsValid() {
		return logger.With(
			zap.Stringer(fields.ActivityTraceIDKey, spanContext.TraceID()),
			zap.Stringer(fields.ActivitySpanIDKey, spanContext.SpanID()),
		)
	}

	return logger
}
