package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func getLog() *zap.Logger {
	_globalRWMutex.RLock()

	if _logger == nil {
		_globalRWMutex.RUnlock()
		return zap.L() // Fallback to zap global logger.
	}

	_globalRWMutex.RUnlock()

	return _logger.log
}

// getErrorLog returns (*zap.Logger).WithOptions(zap.AddCallerSkip(1))
func getErrorLog() *zap.Logger {
	return getLog().WithOptions(zap.AddCallerSkip(1))
}

// Named returns (*zap.Logger).Named()
func Named(name string) *zap.Logger {
	return getLog().Named(name)
}

// Namespace returns (*zap.Logger).With(Namespace())
func Namespace(namespace string) *zap.SugaredLogger {
	return getLog().With(zap.Namespace(namespace)).Sugar()
}

// Info executes (*zap.Logger).Sugar().Info()
func Info(args ...interface{}) { getLog().Sugar().Info(args...) }

// Infof executes (*zap.Logger).Sugar().Infof()
func Infof(template string, args ...interface{}) { getLog().Sugar().Infof(template, args...) }

// Infow executes (*zap.SugaredLogger).Infow()
func Infow(template string, fields ...zap.Field) { getLog().Info(template, fields...) }

// Debug executes (*zap.Logger).Sugar().Debug()
func Debug(args ...interface{}) { getLog().Sugar().Debug(args...) }

// Debugf executes (*zap.Logger).Sugar().Debugf()
func Debugf(template string, args ...interface{}) { getLog().Sugar().Debugf(template, args...) }

// Debugw executes (*zap.SugaredLogger).Debugw()
func Debugw(template string, fields ...zap.Field) { getLog().Debug(template, fields...) }

// Warn executes (*zap.Logger).Sugar().Warn()
func Warn(args ...interface{}) { getLog().Sugar().Warn(args...) }

// Warnf executes (*zap.Logger).Sugar().Warnf()
func Warnf(template string, args ...interface{}) { getLog().Sugar().Warnf(template, args...) }

// Warnw executes (*zap.SugaredLogger).Warnw()
func Warnw(template string, fields ...zap.Field) { getLog().Warn(template, fields...) }

// Error executes (*zap.Logger).Sugar().Error()
func Error(args ...interface{}) { getErrorLog().Sugar().Error(args...) }

// Errorf executes (*zap.Logger).Sugar().Errorf()
func Errorf(template string, args ...interface{}) { getErrorLog().Sugar().Errorf(template, args...) }

// Errorw executes (*zap.SugaredLogger).Errorw()
func Errorw(template string, fields ...zap.Field) { getErrorLog().Error(template, fields...) }

// Fatal executes (*zap.Logger).Sugar().Fatal()
func Fatal(args ...interface{}) { getLog().Sugar().Fatal(args...) }

// Fatalf executes (*zap.Logger).Sugar().Fatalf()
func Fatalf(template string, args ...interface{}) { getLog().Sugar().Fatalf(template, args...) }

// Fatalw executes (*zap.SugaredLogger).Fatalw()
func Fatalw(template string, fields ...zap.Field) { getLog().Fatal(template, fields...) }

// Check returns (*zap.Logger).Check()
func Check(lvl zapcore.Level, msg string) *zapcore.CheckedEntry {
	return getLog().Check(lvl, msg)
}

// With returns (*zap.Logger).With()
func With(fields ...zap.Field) *zap.Logger { return getLog().With(fields...) }

// Sync returns (*zap.Logger).Sync()
func Sync() error { return getLog().Sync() }
