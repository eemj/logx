package log

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type CoreBuilder func(lvl zap.AtomicLevel) zapcore.Core

func ConsoleCore(lvl zap.AtomicLevel) zapcore.Core {
	consoleErrors := zapcore.Lock(os.Stderr)
	consoleDebugging := zapcore.Lock(os.Stdout)

	highPriority := zap.LevelEnablerFunc(func(l zapcore.Level) bool {
		return lvl.Enabled(l) && l >= zapcore.ErrorLevel
	})

	lowPriority := zap.LevelEnablerFunc(func(l zapcore.Level) bool {
		return lvl.Enabled(l) && l < zapcore.ErrorLevel
	})

	consoleEncoder := zapcore.NewConsoleEncoder(NewEncoderConfig())

	return zapcore.NewTee(
		zapcore.NewCore(consoleEncoder, consoleErrors, highPriority),
		zapcore.NewCore(consoleEncoder, consoleDebugging, lowPriority),
	)
}

func JSONCore(syncs ...zapcore.WriteSyncer) CoreBuilder {
	return func(lvl zap.AtomicLevel) zapcore.Core {
		return zapcore.NewCore(
			zapcore.NewJSONEncoder(NewEncoderConfig()),
			zapcore.NewMultiWriteSyncer(syncs...),
			lvl,
		)
	}
}
