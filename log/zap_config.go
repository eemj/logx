package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// NewConfig is an opinionated production config used throughout
// self-developed applications. It uses a JSON encoder, writes to standard
// error, and enables sampling. Stacktraces are automatically included on
// logs of ErrorLevel and above.
func NewConfig(level zapcore.Level) zap.Config {
	return zap.Config{
		Level:       zap.NewAtomicLevelAt(level),
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    NewEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}
}

// NewEncoderConfig is an opinionated production encoding config
// used throughout self-developed applications. Rather then the default ISO8601
// it uses RFC3339 with nano seconds. Durations are also encoded with a
// nano second represetation rather than the default second.
func NewEncoderConfig() zapcore.EncoderConfig {
	return zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.RFC3339NanoTimeEncoder,
		EncodeDuration: zapcore.NanosDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
}
