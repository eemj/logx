module go.jamie.mt/logx

go 1.18

require (
	github.com/grpc-ecosystem/go-grpc-middleware/v2 v2.0.0-rc.4
	go.opentelemetry.io/otel/trace v1.14.0
	go.uber.org/zap v1.24.0
	google.golang.org/grpc v1.54.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	go.opentelemetry.io/otel v1.14.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230331144136-dcfb400f0633 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
