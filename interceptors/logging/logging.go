package logging // import "go.jamie.mt/logx/interceptors/logging"

import (
	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type LoggingInterceptor struct {
	opts *options
}

// New will initlize the gRPC zap logger.
// If options aren't provided it will use the global logger for zap (`zap.L()`)
// and it won't skip anything.
func New(opts ...Option) *LoggingInterceptor {
	o := new(options)

	for _, opt := range opts {
		opt(o)
	}

	if o.skipper == nil {
		o.skipper = DefaultSkipper
	}
	if o.logger == nil {
		o.logger = zap.L()
	}

	return &LoggingInterceptor{opts: o}
}

func (z *LoggingInterceptor) UnaryServerInterceptor() grpc.UnaryServerInterceptor {
	return interceptors.UnaryServerInterceptor(&reportable{z.opts})
}

func (z *LoggingInterceptor) StreamServerInterceptor() grpc.StreamServerInterceptor {
	return interceptors.StreamServerInterceptor(&reportable{z.opts})
}

func (z *LoggingInterceptor) UnaryClientInterceptor() grpc.UnaryClientInterceptor {
	return interceptors.UnaryClientInterceptor(&reportable{z.opts})
}

func (z *LoggingInterceptor) StreamClientInterceptor() grpc.StreamClientInterceptor {
	return interceptors.StreamClientInterceptor(&reportable{z.opts})
}
