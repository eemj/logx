package logging

import (
	"context"
	"io"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors"
	"go.jamie.mt/logx/fields"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc/status"
)

func commonFields(
	kind string,
	callMeta interceptors.CallMeta,
) []zap.Field {
	return []zap.Field{
		zap.String(fields.SystemFieldKey, fields.SystemFieldValue),
		zap.String(fields.KindFieldKey, kind),
		zap.String(fields.GrpcMethodKey, callMeta.Method),
		zap.String(fields.GrpcMethodTypeKey, string(callMeta.Typ)),
		zap.String(fields.GrpcServiceKey, callMeta.Service),
	}
}

type reportable struct {
	opts *options
}

func (r *reportable) reporter(
	ctx context.Context,
	kind string,
	callMeta interceptors.CallMeta,
) (interceptors.Reporter, context.Context) {
	zapFields := commonFields(kind, callMeta)
	zapFields = append(zapFields, zap.Time(fields.GrpcStartTimeKey, time.Now().UTC()))

	if deadline, ok := ctx.Deadline(); ok {
		zapFields = append(zapFields, zap.Time(fields.GrpcRequestDeadlineKey, deadline))
	}

	spanContext := trace.SpanContextFromContext(ctx)

	if spanContext.IsValid() {
		zapFields = append(
			zapFields,
			zap.Stringer(fields.ActivityTraceIDKey, spanContext.TraceID()),
			zap.Stringer(fields.ActivitySpanIDKey, spanContext.SpanID()),
		)
	}

	return &reporter{
		callMeta: callMeta,
		logger:   r.opts.logger.With(zapFields...),
		skipper:  r.opts.skipper,
	}, ctx
}

func (r *reportable) ServerReporter(
	ctx context.Context,
	callMeta interceptors.CallMeta,
) (interceptors.Reporter, context.Context) {
	return r.reporter(ctx, fields.KindServerValue, callMeta)
}

func (r *reportable) ClientReporter(
	ctx context.Context,
	callMeta interceptors.CallMeta,
) (interceptors.Reporter, context.Context) {
	return r.reporter(ctx, fields.KindClientValue, callMeta)
}

type reporter struct {
	callMeta interceptors.CallMeta
	logger   *zap.Logger
	skipper  Skipper
}

func (r *reporter) PostCall(err error, duration time.Duration) {
	if r.skipper(r.callMeta.Service, r.callMeta.Method, err) {
		return
	}

	if err == io.EOF {
		err = nil
	}

	r.logger = r.logger.With(
		zap.Stringer(fields.GrpcCodeKey, status.Code(err)),
		zap.Duration(fields.GrpcDurationKey, duration),
	)

	if err == nil {
		r.logger.Info(fields.GrpcCallZapMessage)
	} else {
		r.logger.Error(fields.GrpcCallZapMessage, zap.Error(err))
	}
}

func (*reporter) PostMsgSend(interface{}, error, time.Duration) {}

func (*reporter) PostMsgReceive(interface{}, error, time.Duration) {}
