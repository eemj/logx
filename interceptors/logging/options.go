package logging

import "go.uber.org/zap"

var (
	DefaultSkipper = Skipper(func(string, string, error) bool { return false })
)

// Skipper defines the type for a callback function
// in which one should pass a condition where a log entry
// will get skipped.
type Skipper func(service, method string, err error) (skip bool)

type options struct {
	logger  *zap.Logger
	skipper Skipper
}

type Option func(*options)

func WithSkipper(skipper Skipper) Option {
	return func(o *options) {
		o.skipper = skipper
	}
}

func WithLogger(logger *zap.Logger) Option {
	return func(o *options) {
		o.logger = logger
	}
}
