package fields // import "go.jamie.mt/logx/fields"

const (
	// KindServerValue return `server`
	KindServerValue = "server"
	// KindClientValue returns `client`
	KindClientValue = "client"
	// KindFieldKey returns `kind`
	KindFieldKey = "kind"

	// SystemFieldKey returns `system`
	SystemFieldKey = "system"
	// SystemFieldValue returns `grpc`
	SystemFieldValue = "grpc"

	// GrpcCodeKey returns `grpc.code`
	GrpcCodeKey = "grpc.code"
	// GrpcDurationKey returns `grpc.duration`
	GrpcDurationKey = "grpc.duration"
	// GrpcMethodKey returns `grpc.method`
	GrpcMethodKey = "grpc.method"
	// GrpcMethodTypeKey returns `grpc.method_type`
	GrpcMethodTypeKey = "grpc.method_type"
	// GrpcServiceKey returns `grpc.service`
	GrpcServiceKey = "grpc.service"
	// GrpcStartTimeKey returns `grpc.start_time`
	GrpcStartTimeKey = "grpc.start_time"
	// GrpcRequestDeadlineKey returns `grpc.request.deadline`
	GrpcRequestDeadlineKey = "grpc.request.deadline"

	// GrpcCallZapMessage returns `grpc.call`
	GrpcCallZapMessage = "grpc.call"

	// ActivityTraceIDKey returns `activity.trace.id`
	ActivityTraceIDKey = "activity.trace.id"

	// ActivitySpanIDKey returns `activity.span.id`
	ActivitySpanIDKey = "activity.span.id"
)
